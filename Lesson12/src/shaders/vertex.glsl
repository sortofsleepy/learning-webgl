
attribute vec3 aPosition;
attribute vec2 aTextureCoord;
attribute vec3 aVertexNormal;


uniform mat4 uModelView;
uniform mat4 uProjection;
uniform mat3 uNormalMatrix;
uniform vec3 uAmbientColor;
uniform vec3 uLightingDirection;
uniform vec3 uDirectionalColor;

varying vec2 vTextureCoord;
varying vec3 vNormal;
varying vec3 vLightWeighting;

void main(void) {
      gl_Position = uProjection * uModelView * vec4(aPosition, 1.0);
      vTextureCoord = aTextureCoord;
      vNormal = aVertexNormal;

      vec3 transformedNormal = uNormalMatrix * aVertexNormal;
      float directionalLightWeighting = max(dot(transformedNormal, uLightingDirection), 0.0);
      vLightWeighting = uAmbientColor + uDirectionalColor * directionalLightWeighting;
    //vLightWeighting = uAmbientColor;
}