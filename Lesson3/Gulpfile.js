var gulp = require("gulp");
var gutil = require("gulp-util");
var sourcemaps = require("gulp-sourcemaps");
var source = require("vinyl-source-stream");
var buffer = require("vinyl-buffer");
var watchify = require("watchify");
var browserify = require("browserify");
var babelify = require("babelify");
var browserSync = require("browser-sync");

////////////// JS SETUP ///////////////////////
//setup entry point for js
var bundler = watchify(browserify('./src/Main.js',watchify.args));
bundler.transform(babelify);

gulp.task('js',bundle);
bundler.on('update',bundle);
/**
 * Bundles JS together
 * @returns {*}
 */
function bundle(){
    browserSync.reload();
    return bundler.bundle()
        .on("error",gutil.log.bind(gutil,"Browserify Error"))
        .pipe(source('./bundle.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps:false}))
        .pipe(sourcemaps.write("./"))
        .pipe(gulp.dest("./public/js"));
}


gulp.task("default",["js"],function(){
    browserSync({
        server:{baseDir:"./public"}
        // proxy:"localhost:3000"
    });

    gulp.watch(["./src/**/*.js"],["js"]);
});
/**
 * Created by sortofsleepy on 11/4/15.
 */
