var gl = require('webgl-context')({
    width:window.innerWidth,
    height:window.innerHeight
});
var createBuffer = require('gl-buffer');
var createVao = require('gl-vao');
var mat4 = require('gl-mat4');
var createShader = require('gl-shader');
var glslify = require('glslify');


document.body.appendChild(gl.canvas);

var shader = createShader(gl,glslify('src/shaders/vertex.glsl'),
    glslify('src/shaders/fragment.glsl'));

// build "camera" matrix
var projection = mat4.create();
mat4.perspective(projection, Math.PI / 4, window.innerWidth/window.innerHeight, 0.1, 100)

//build shapes
var triangle = createVao(gl,[
    {
        buffer:createBuffer(gl,[
            +0.0, +1.0, +0.0,
            -1.0, -1.0, +0.0,
            +1.0, -1.0, +0.0
        ]),
        type:gl.FLOAT,
        size:3
    }
]);
//build a matrix for the triangle so that we can do translation,scaling,rotation etc.
var triangleMatrix = mat4.create();

//value we increment for rotation
var angle = 0.0;

//animate everything
animate();
function animate(){
    requestAnimationFrame(animate);

    //set the clear color
    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    //set clear flags
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    //set viewport dimensions
    gl.viewport(0,0,window.innerWidth,window.innerHeight);

    //center the rotation of the matrix by reseting matrix to initial position each frame
    mat4.identity(triangleMatrix, triangleMatrix);

    //translate z position a bit
    mat4.translate(triangleMatrix, triangleMatrix, [0, 0, -7]);

    //rotate the matrix by increasing angle and rotate along y axis
    mat4.rotate(triangleMatrix,triangleMatrix,angle,[0,1,0]);

    //bind shader
    shader.bind();

    //enable pointer to position attribute
    shader.attributes.aPosition.pointer();

    //send "camera" matrix to shader
    shader.uniforms.uProjection = projection;

    //bind the triangle vbo
    triangle.bind();

    //mind the model matrix for the triangle shape
    shader.uniforms.uModelView = triangleMatrix;

    //draw the triangles
    triangle.draw(gl.TRIANGLES,3);

    //increment incrementor value
    angle += 0.01;
}