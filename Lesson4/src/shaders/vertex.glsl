precision mediump float;

attribute vec3 aPosition;
attribute vec2 uv;

uniform mat4 uModelView;
uniform mat4 uProjection;

 varying vec2 vTextureCoord;


void main() {
  vTextureCoord = uv;
  vTextureCoord = vec2(0.0,1.0)+vec2(0.5,-0.5) * (aPosition.xy + 1.0);
  gl_Position = uProjection * uModelView * vec4(aPosition, 1.0);
}