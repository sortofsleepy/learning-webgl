# Learning webgl #

this repo contains code that follows aloing with the lessons found at 
[LearningWebGL](http://learningwebgl.com) using modules from [stack.gl](http://stack.gl)

While I do already know a little bit of OpenGL and WebGL, it's always been from the context of a
framework of some kind which can sometimes hide some of the actual code behind "magic" functions. 
This is here to remind myself of how to do things as well as hopefully become a resource to others.