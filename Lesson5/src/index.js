import Texture from "./Texture"

var gl = require('webgl-context')({
    width:window.innerWidth,
    height:window.innerHeight
});
var createBuffer = require('gl-buffer');
var createVao = require('gl-vao');
var mat4 = require('gl-mat4');
var createShader = require('gl-shader');
var glslify = require('glslify');

document.body.appendChild(gl.canvas);

var shader = createShader(gl,glslify('./shaders/vertex.glsl'),
    glslify('./shaders/fragment.glsl'));

// build "camera"
var projection = mat4.create();
mat4.perspective(projection, Math.PI / 4, window.innerWidth/window.innerHeight, 0.1, 100)

var elementBuffer = createBuffer(gl,[
    0, 1, 2,      0, 2, 3,    // Front face
    4, 5, 6,      4, 6, 7,    // Back face
    8, 9, 10,     8, 10, 11,  // Top face
    12, 13, 14,   12, 14, 15, // Bottom face
    16, 17, 18,   16, 18, 19, // Right face
    20, 21, 22,   20, 22, 23  // Left face
],gl.ELEMENT_ARRAY_BUFFER);

////////// BUILD TEXTURE COORDINATE BUFFER ///////////////

var texCoordBuffer = createBuffer(gl,[
    // Front face
    0.0, 0.0,
    1.0, 0.0,
    1.0, 1.0,
    0.0, 1.0,

    // Back face
    1.0, 0.0,
    1.0, 1.0,
    0.0, 1.0,
    0.0, 0.0,

    // Top face
    0.0, 1.0,
    0.0, 0.0,
    1.0, 0.0,
    1.0, 1.0,

    // Bottom face
    1.0, 1.0,
    0.0, 1.0,
    0.0, 0.0,
    1.0, 0.0,

    // Right face
    1.0, 0.0,
    1.0, 1.0,
    0.0, 1.0,
    0.0, 0.0,

    // Left face
    0.0, 0.0,
    1.0, 0.0,
    1.0, 1.0,
    0.0, 1.0,
]);

//load texture
var tex = new Texture(gl);
tex.loadTexture("/img/download.gif");


///////////////


//build shapes
var triangle = createVao(gl,[
    {
        buffer:createBuffer(gl,[
            // Front face
            -1.0, -1.0,  1.0,
            1.0, -1.0,  1.0,
            1.0,  1.0,  1.0,
            -1.0,  1.0,  1.0,

            // Back face
            -1.0, -1.0, -1.0,
            -1.0,  1.0, -1.0,
            1.0,  1.0, -1.0,
            1.0, -1.0, -1.0,

            // Top face
            -1.0,  1.0, -1.0,
            -1.0,  1.0,  1.0,
            1.0,  1.0,  1.0,
            1.0,  1.0, -1.0,

            // Bottom face
            -1.0, -1.0, -1.0,
            1.0, -1.0, -1.0,
            1.0, -1.0,  1.0,
            -1.0, -1.0,  1.0,

            // Right face
            1.0, -1.0, -1.0,
            1.0,  1.0, -1.0,
            1.0,  1.0,  1.0,
            1.0, -1.0,  1.0,

            // Left face
            -1.0, -1.0, -1.0,
            -1.0, -1.0,  1.0,
            -1.0,  1.0,  1.0,
            -1.0,  1.0, -1.0,
        ]),
        type:gl.FLOAT,
        size:3
    },
    [0.8, 1, 0.5],
],elementBuffer);

var triangleMatrix = mat4.create();
console.log(texCoordBuffer);
animate();
var angle = 0.0;
function animate(){
    requestAnimationFrame(animate);
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    gl.viewport(0,0,window.innerWidth,window.innerHeight);


    mat4.identity(triangleMatrix, triangleMatrix);
    mat4.translate(triangleMatrix, triangleMatrix, [0, 0, -7]);

    //rotate the matrix by increasing angle and rotate along y axis
    mat4.rotate(triangleMatrix,triangleMatrix,angle,[1,1,0]);

    //bind the shader
    shader.bind();

    //make sure we send position attribute
    shader.attributes.aPosition.pointer();

    //make sure projection and model view matrices are kept up to date
    shader.uniforms.uProjection = projection;
    shader.uniforms.uModelView = triangleMatrix;

    //bind the triangle vao
    triangle.bind();

    //bind texture coordinates
    texCoordBuffer.bind();
    shader.attributes.aTextureCoord.pointer();

    //bind and send the texture
    var texture = tex.bind();
    shader.uniforms.uSampler = texture;

    //draw everything
    triangle.draw(gl.TRIANGLES,36);
    triangle.unbind();

    //increment incrementor value
    angle += 0.01;
}