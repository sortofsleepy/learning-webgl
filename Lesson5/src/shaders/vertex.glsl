attribute vec3 aPosition;
attribute vec2 aTextureCoord;
uniform mat4 uModelView;
uniform mat4 uProjection;

varying vec2 vTextureCoord;


void main(void) {
      gl_Position = uProjection * uModelView * vec4(aPosition, 1.0);
      vTextureCoord = aTextureCoord;
}