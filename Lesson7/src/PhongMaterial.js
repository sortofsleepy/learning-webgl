var Vector = require("nitro-math").Vector;
var createBuffer = require("gl-buffer");
var mat3 = require('gl-mat3');
var mat4 = require('gl-mat4');
import Texture from "./Texture"

class PhongMaterial extends Texture {
    constructor(context,lightDirUniformName,lightColorUniformName,ambinentColorUniformName){
        super(context);

        this.normalMatrix = mat3.create();
        this.normalMatrix = mat3.identity(this.normalMatrix);
        this.gl = context;
        this.reflectLight = true;

        this.ambientLightColor = new Vector();
        this.lightColor = new Vector();
        this.lightDirection = new Vector();

        this.lightDirectionUniformName = lightDirUniformName !== undefined ? lightDirUniformName : "uLightingDirection";
        this.lightColorUniformName = lightColorUniformName !== undefined ? lightColorUniformName : "uDirectionalColor";
        this.ambinentColorUniformName = ambinentColorUniformName !== undefined ? ambinentColorUniformName : "uAmbientColor"
    }

    /**
     * Calculates the normal matrix to use in the lighting
     * @param modelViewMatrix the model-view matrix to base the calculations on
     */
    calculateNormalMatrix(modelViewMatrix,shader){
        var normalMatrix = mat3.create();
        mat3.fromMat4(modelViewMatrix, normalMatrix);
        mat3.transpose(normalMatrix,normalMatrix);
       //shader.uniforms["uNormalMatrix"] = normalMatrix;
        return normalMatrix;
    }

    /**
     * Gets a reference to the attribute this material is associated with
     * @param shader the shader object to use
     * @param locationName
     */
    getAttribLocation(shader,locationName){

        var gl = this.gl;
        locationName = locationName !== undefined ? locationName : "aVertexNormal";
        this.shader = shader;



        if(shader.hasOwnProperty("attributes")){
           if(shader.attributes.hasOwnProperty(locationName)){
               this.attrib = shader.attributes[locationName].location
           }else{
               this.attrib = gl.getAttribLocation(shader.program,locationName);
           }
        }else{
            try{
                this.attrib = gl.getAttribLocation(shader,locationName);
            }catch(e){
                console.error("PhongMaterial::getAttribLocation error : unknown shader object");
            }
        }

        if((this.attrib !== null) || (this.attrib !== undefined)){
            gl.enableVertexAttribArray(this.attrib);
        }else {
            console.error("Unable to obtain and enable attribute location");
        }

    }

    /**
     * Sets up the normals for lighting
     * @param normalsArray
     */
    setNormals(normalsArray){
        this.normals = createBuffer(this.gl,normalsArray);
    }

    /**
     * Toggles whether or not to calculating lighting.
     * @returns {PhongMaterial}
     */
    toggleLight(){
        if(!this.reflectLight){
            this.reflectLight = true;
        }else{
            this.reflectLight = false;
        }
        return this;
    }

    /**
     * Sets the direction of the light
     * @param x
     * @param y
     * @param z
     */
    setLightDirection(x,y,z){
        this.lightDirection.x = x;
        this.lightDirection.y = y;
        this.lightDirection.z = z;
        this.lightDirection.normalize();

        this.lightDirectionUniform = this._toUniform(this.lightDirection);
        return this;
    }

    /**
     * Sets the color of the light. Will automatically normalize values
     * @param x
     * @param y
     * @param z
     * @returns {PhongMaterial}
     */
    setLightingColor(x,y,z){
        this.lightColor.x = x / 255.0;
        this.lightColor.y = y / 255.0;
        this.lightColor.z = z / 255.0;

        this.lightColorUniform = this._toUniform(this.lightColor);
        return this;
    }

    setAmbientLightColor(x,y,z){
        this.ambientLightColor.x = x / 255.0;
        this.ambientLightColor.y = y / 255.0;
        this.ambientLightColor.z = z / 255.0;

        this.ambientLightUniform = this._toUniform(this.ambientLightColor);
        console.log(this.ambientLightUniform)
        return this;
    }

    getLightingColor(){
        return this.lightColor;
    }

    getLightDirection(){
        return this.lightDirection;
    }

    bind(unit,shader){
        var gl = this.gl;

        /**
         * If we want lighting to affect the material and we've set a normal buffer,
         * bind the normals buffer
         */
        if(this.reflectLight){
            if(this.hasOwnProperty("normals")){
                this.normals.bind();
            }
        }

        if(unit !== undefined) {
            gl.activeTexture(gl.TEXTURE0 + (unit|0))
        }
        gl.bindTexture(gl.TEXTURE_2D, this.texture);

        //bind lighting uniforms
        if(shader !== undefined){
            shader.uniforms.uAmbientColor = [0.2,0.2,0.2];
            shader.uniforms.uLightingDirection = [-0.25,-0.25,-1.0];
            shader.uniforms.uDirectionalColor = [0.8,0.8,0.8]
        }

        return gl.getParameter(gl.ACTIVE_TEXTURE) - gl.TEXTURE0
    }

    /**
     * Internal method to prepare vectors for use with gl-shader
     * @param vec Vector object to convert
     * @returns {*[]}
     * @private
     */
    _toUniform(vec){
        return [vec.x,vec.y,vec.z]
    }

}

export default PhongMaterial