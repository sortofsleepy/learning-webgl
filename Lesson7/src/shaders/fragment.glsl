precision mediump float;
varying vec2 vTextureCoord;
varying vec3 vLightWeighting;

uniform sampler2D uSampler;
void main(void) {
    vec4 tex = texture2D(uSampler, vTextureCoord);
    vec4 color = vec4(1.0,1.0,0.0,1.0);
    gl_FragColor = vec4(tex.rgb * vLightWeighting,tex.a);
}