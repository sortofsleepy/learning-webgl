/**
 * Simple class for dealing with 2D textures. Largely based off of
 * gl-texture2d from stack-gl
 * https://github.com/stackgl/gl-texture2d/blob/master/texture.js
 */
class Texture {
    constructor(context,options){

        var defaults = {
            flipTexture:false
        };

        var gl = context !== undefined ? context : (function(){
            if(window.debug){
                console.error("Texture class needs a WebGL context in order to function");
            }
        });
        this.gl = gl;

        //create and init texture
        this.texture = gl.createTexture();
        this._initTexture(this.texture);



        this._mipLevels = [0];
        this._magFilter = gl.NEAREST;
        this._minFilter = gl.NEAREST;
        this._wrapS = gl.CLAMP_TO_EDGE;
        this._wrapT = gl.CLAMP_TO_EDGE;

    }
    _initTexture(tex){
        var gl = this.gl;
        gl.bindTexture(gl.TEXTURE_2D, tex);
        //http://stackoverflow.com/questions/19722247/webgl-wait-for-texture-to-load
        //create temp texture data to satisfy complaint of invalid texture
        gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,1,1,0,gl.RGBA,gl.UNSIGNED_BYTE,new Uint8Array([255,0,0,255]));
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    }

    /**
     * Deals with loading image into texture data
     * @param obj some kind of DOM based image, either canvas, image element, video element
     * or ImageData object
     */
    loadTexture(obj){
        var gl = this.gl;
        var texture = this.texture;
        var self = this;
        var img = new Image();


        //precheck to see if we've entered a url
        if(typeof obj === "string"){
            console.log("loading image");
            img.src = obj;
        }
        //check size of texture
        var maxSize = gl.getParameter(gl.MAX_TEXTURE_SIZE);



        if(obj instanceof HTMLCanvasElement ||
            obj instanceof HTMLImageElement ||
            obj instanceof HTMLVideoElement ||
            obj instanceof ImageData) {

            /**
             * Do quick check of whether or not we've exceeded the max height for this computer
             */
            if(obj instanceof HTMLCanvasElement ||
                obj instanceof HTMLImageElement ||
                obj instanceof HTMLVideoElement){

                var area = obj.width * obj.height;
                if(area < maxSize){
                    gl.texImage2D(gl.TEXTURE_2D, 0,gl.RGBA,gl.RGBA,gl.UNSIGNED_BYTE, obj);
                }

            }else if (obj instanceof ImageData){
                if(obj.length < maxSize){
                    gl.texImage2D(gl.TEXTURE_2D, 0,gl.RGBA,gl.RGBA,gl.UNSIGNED_BYTE, obj);
                }
            }

        } else if(obj.shape && obj.data && obj.stride) {
            //TODO add texture creation from array of data
        }else{

            img.onload = function(){

                //bind the texture temporarily
                gl.bindTexture(gl.TEXTURE_2D, texture);
                gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
                gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, img);
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
                gl.bindTexture(gl.TEXTURE_2D,null);
            }
        }


    }

    bind(unit){
        var gl = this.gl;
        if(unit !== undefined) {
            gl.activeTexture(gl.TEXTURE0 + (unit|0))
        }
        gl.bindTexture(gl.TEXTURE_2D, this.texture);
        if(unit !== undefined) {
            return (unit|0)
        }
        return gl.getParameter(gl.ACTIVE_TEXTURE) - gl.TEXTURE0
    }

    unbind(){
        this.gl.bindTexture(this.gl.TEXTURE_2D, null);
    }

    /**
     * Checks to see if the value is a power of 2
     * @param value the value we want to examine
     * @returns {boolean}
     * @private
     */
    _isPowerOf2(value){
        return (value & (value - 1)) == 0;
    }
}

export default Texture;