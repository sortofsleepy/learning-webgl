var gl = require('webgl-context')({
    width:window.innerWidth,
    height:window.innerHeight
});
var createBuffer = require('gl-buffer');
var createVao = require('gl-vao');
var mat4 = require('gl-mat4');
var createShader = require('gl-shader');
var glslify = require('glslify');

console.log(gl);
document.body.appendChild(gl.canvas);

var shader = createShader(gl,glslify('src/shaders/vertex.glsl'),
    glslify('src/shaders/fragment.glsl'));

console.log(shader.uniforms);
// build "camera"
var projection = mat4.create();
mat4.perspective(projection, Math.PI / 4, window.innerWidth/window.innerHeight, 0.1, 100)

//build shapes
var triangle = createVao(gl,[
    {
        buffer:createBuffer(gl,[
            +0.0, +1.0, +0.0,
            -1.0, -1.0, +0.0,
            +1.0, -1.0, +0.0
        ]),
        type:gl.FLOAT,
        size:3
    },
    [0.8, 1, 0.5]
]);
var triangleMatrix = mat4.create();

animate();
function animate(){
    requestAnimationFrame(animate);
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    gl.viewport(0,0,window.innerWidth,window.innerHeight);


    mat4.identity(triangleMatrix, triangleMatrix);
    mat4.translate(triangleMatrix, triangleMatrix, [0, 0, -7]);

    shader.bind();
    shader.attributes.aPosition.pointer();
    shader.uniforms.uProjection = projection;

    triangle.bind();
    shader.uniforms.uModelView = triangleMatrix;
    triangle.draw(gl.TRIANGLES,3);

}