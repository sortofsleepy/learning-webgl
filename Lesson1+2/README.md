# Lesson 1 + 2 #

This contains code that reflects lesson 1 and 2 on learningwebgl.com using libraries
pulled together from [stack.gl](http://stack.gl). Its kinda a mish-mash of actual lesson code
with similar code already put together by the team as well as some combinations of my own,
namely, with this code, theres no square and we color the triangle in the shader
